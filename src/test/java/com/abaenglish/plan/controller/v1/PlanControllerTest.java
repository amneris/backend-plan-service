package com.abaenglish.plan.controller.v1;

import com.abaenglish.external.abawebapps.domain.CountryIso;
import com.abaenglish.external.abawebapps.service.IAbawebappsExternalService;
import com.abaenglish.external.selligent.service.ISelligentSegmentService;
import com.abaenglish.external.zuora.rest.domain.catalog.Pricing;
import com.abaenglish.external.zuora.rest.service.IZuoraExternalServiceRest;
import com.abaenglish.plan.PlanApplication;
import com.abaenglish.user.controller.v1.dto.response.CurrencyApiResponse;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.config.JsonConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.specification.ResponseSpecification;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static com.abaenglish.plan.objectmother.CatalogObjectMother.defaultCatalog;
import static com.abaenglish.plan.objectmother.CatalogObjectMother.defaultCatalogABA;
import static com.abaenglish.plan.objectmother.CurrencyObjectMother.currencyUSD;
import static com.abaenglish.plan.objectmother.CurrencyObjectMother.currencyUSDExternal;
import static com.abaenglish.plan.objectmother.ListPlanObjectMother.defaultMalformedSelligentListPlans;
import static com.abaenglish.plan.objectmother.ListPlanObjectMother.defaultSelligentPlansABA;
import static io.restassured.RestAssured.when;
import static java.lang.String.format;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PlanApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PlanControllerTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Value("${local.server.port}")
    int port;
    CountryIso countryIso = CountryIso.Builder.aCountryIso().idCountry(301L).abaCountryIso("ABA").build();
    @MockBean
    private IZuoraExternalServiceRest zuoraExternalService;
    @MockBean
    private IAbawebappsExternalService abawebappsExternalService;
    @MockBean
    private ISelligentSegmentService selligentSegmentService;
    @MockBean
    private IUserServiceFeign userServiceFeign;

    @After
    public void tearDown() {
        RestAssured.reset();
    }

    @Before
    public void setup() {

        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
        RestAssured.config = RestAssuredConfig.config().jsonConfig(JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
    }

    @Test
    public void testIllegalArgumentExceptions() throws IllegalArgumentException { // , QueuesNotAvailableException, AmqpIllegalStateException {

        thrown.expect(IllegalArgumentException.class);
        getPriceList();
    }

    private void getPriceList() {

        final Long userId = 1L;
        final Integer segmentId = 1;

        Mockito.when(selligentSegmentService.getSelligentSegment(anyInt())).thenReturn(defaultMalformedSelligentListPlans());
        Mockito.when(abawebappsExternalService.getCurrency(anyLong())).thenReturn(currencyUSDExternal());
        Mockito.when(zuoraExternalService.getCatalog()).thenReturn(defaultCatalog());

        ResponseSpecBuilder ABAbuilder = ABAresponseDefaultMalformedCatalog(currencyUSD());
        ResponseSpecification responseSpec = ABAbuilder.build();

        when().

                get(format("/api/v1/plans?user=%d&segment=%d", userId, segmentId)).
                then().
                spec(responseSpec);

    }

    private ResponseSpecBuilder ABAresponseDefaultMalformedCatalog(CurrencyApiResponse currency) {

        ResponseSpecBuilder builder = new ResponseSpecBuilder();

        for (int i = 0; i < 4; i++) {

            if (i == 1 || i == 3) {
                builder.expectBody("plans[" + i + "]", equalTo(null));
            } else {
                BigDecimal price = null;
                BigDecimal discountPercentage = null;
                BigDecimal discount = null;
                BigDecimal discountMonthly = null;

                for (Pricing d : defaultCatalog().getProducts().get(0).getProductRatePlans().get(i).getRatePlanCharges().get(0).getPricing()) {

                    if (d.getCurrency() != null && d.getCurrency().contains("EUR")) {
                        price = d.getPrice();
                        discountPercentage = d.getDiscountPercentage();
                    } else if (d.getCurrency().contains(currency.getAbaIdCurrency()) && d.getCurrency() != null) {
                        price = d.getPrice();
                        discountPercentage = d.getDiscountPercentage();
                    }
                }

                BigDecimal montlhy = price.divide(new BigDecimal(defaultCatalogABA().getProducts().get(0).getProductRatePlans().get(i).getPeriod()), 2, BigDecimal.ROUND_DOWN);

                builder.expectBody("plans[" + i + "].id", is(defaultSelligentPlansABA().getPlanSelligent().get(i).getPrice()));
                builder.expectBody("plans[" + i + "].prices.promo.id", is(defaultSelligentPlansABA().getPlanSelligent().get(i).getDiscount()));
                builder.expectBody("plans[" + i + "].prices.monthlyBase", equalTo(montlhy));
                builder.expectBody("plans[" + i + "].prices.base", equalTo(price));

                builder.expectBody("plans[" + i + "].id", is(defaultCatalogABA().getProducts().get(0).getProductRatePlans().get(i).getId()));
                builder.expectBody("plans[" + i + "].titleKey", is(defaultCatalogABA().getProducts().get(0).getProductRatePlans().get(i).getKey()));
                builder.expectBody("plans[" + i + "].featured", is(defaultCatalogABA().getProducts().get(0).getProductRatePlans().get(i).getFeatured()));
                builder.expectBody("plans[" + i + "].period", is(Integer.parseInt(defaultCatalogABA().getProducts().get(0).getProductRatePlans().get(i).getPeriod())));

                if (discountPercentage != null) {
                    discount = price.subtract(price.multiply(discountPercentage).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP));
                    discountMonthly = montlhy.subtract(montlhy.multiply(discountPercentage).divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP));
                    builder.expectBody("plans[" + i + "].prices.promo.discount", equalTo(discountPercentage));
                    builder.expectBody("plans[" + i + "].prices.promo.discountBase", equalTo(discount));
                    builder.expectBody("plans[" + i + "].prices.promo.discountMonthly", equalTo(discountMonthly));
                }
            }
        }

        builder.expectStatusCode(200);
        builder.expectBody("currency.iso", is(currency.getAbaIdCurrency()));

        return builder;
    }

}
