package com.abaenglish.plan.objectmother;

import com.abaenglish.external.selligent.domain.ListPlan;
import com.abaenglish.external.selligent.domain.PlanSelligent;

import java.util.ArrayList;
import java.util.List;

public class ListPlanObjectMother {

    public static ListPlan defaultSelligentPlans() {

        List<PlanSelligent> plansSelligent = new ArrayList<>();

        PlanSelligent onePlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 1M")
                .price("2c92c0f9552e6022015530268dd6314a")
                .build();
        plansSelligent.add(onePlanSelligent);

        PlanSelligent sixPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 6M")
                .price("2c92c0f9552e6022015530268ea6316d")
                .build();
        plansSelligent.add(sixPlanSelligent);

        PlanSelligent oneYearPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 12M")
                .price("2c92c0f9552e6022015530268f953190")
                .build();
        plansSelligent.add(oneYearPlanSelligent);

        PlanSelligent twoYearPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 24M")
                .price("2c92c0f9552e60220155302690b631b3")
                .build();
        plansSelligent.add(twoYearPlanSelligent);

        ListPlan listPlan = ListPlan.Builder.aListPlan()
                .planSelligent(plansSelligent)
                .build();

        return listPlan;
    }

    public static ListPlan defaultSelligentPlansABA() {

        List<PlanSelligent> plansSelligent = new ArrayList<>();

        PlanSelligent onePlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 1M")
                .price("301_30|@|301|@|30")
                .build();
        plansSelligent.add(onePlanSelligent);

        PlanSelligent sixPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 6M")
                .price("301_180|@|301|@|180")
                .build();
        plansSelligent.add(sixPlanSelligent);

        PlanSelligent oneYearPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 12M")
                .price("301_360|@|301|@|360")
                .build();
        plansSelligent.add(oneYearPlanSelligent);

        PlanSelligent twoYearPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 24M")
                .price("301_720|@|301|@|720")
                .build();
        plansSelligent.add(twoYearPlanSelligent);

        ListPlan listPlan = ListPlan.Builder.aListPlan()
                .planSelligent(plansSelligent)
                .build();

        return listPlan;
    }

    public static ListPlan discountSelligentPlans() {

        List<PlanSelligent> plansSelligent = new ArrayList<>();

        PlanSelligent onePlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("20% discount - 1M")
                .price("2c92c0f9552e6022015530268dd6314a")
                .build();
        plansSelligent.add(onePlanSelligent);

        PlanSelligent sixPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("20% discount - 6M")
                .price("2c92c0f9552e6022015530268ea6316d")
                .discount("2c92c0f8552e5302015530abaa915b5c")
                .build();
        plansSelligent.add(sixPlanSelligent);

        PlanSelligent oneYearPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("20% discount - 12M")
                .price("2c92c0f9552e6022015530268f953190")
                .discount("2c92c0f8552e530a015530aab0645c31")
                .build();
        plansSelligent.add(oneYearPlanSelligent);

        PlanSelligent twoYearPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("20% discount - 24M")
                .price("2c92c0f9552e60220155302690b631b3")
                .discount("2c92c0f8552e535a015530a8706400fe")
                .build();
        plansSelligent.add(twoYearPlanSelligent);

        ListPlan listPlan = ListPlan.Builder.aListPlan()
                .planSelligent(plansSelligent)
                .build();

        return listPlan;
    }

    public static ListPlan defaultMalformedSelligentListPlans() {

        List<PlanSelligent> plansSelligent = new ArrayList<>();

        PlanSelligent onePlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 1M")
                .price("2c92c0f9552e6022015530268dd6314a")
                .build();
        plansSelligent.add(onePlanSelligent);

        plansSelligent.add(null);

        PlanSelligent twoYearPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 24M")
                .price("2c92c0f9552e60220155302690b631b3")
                .build();
        plansSelligent.add(twoYearPlanSelligent);

        plansSelligent.add(null);

        ListPlan listPlan = ListPlan.Builder.aListPlan()
                .planSelligent(plansSelligent)
                .build();

        return listPlan;
    }

    public static ListPlan defaultMalformedSelligentFiledPlans() {

        List<PlanSelligent> plansSelligent = new ArrayList<>();

        PlanSelligent onePlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name(null)
                .price("2c92c0f9552e6022015530268dd6314a")
                .build();
        plansSelligent.add(onePlanSelligent);

        PlanSelligent twoYearPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 24M")
                .price(null)
                .build();
        plansSelligent.add(twoYearPlanSelligent);

        ListPlan listPlan = ListPlan.Builder.aListPlan()
                .planSelligent(plansSelligent)
                .build();

        return listPlan;
    }

    public static ListPlan discountSelligentMalformedDefaultPlans() {

        List<PlanSelligent> plansSelligent = new ArrayList<>();

        PlanSelligent oneMonthPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 1M")
                .price("c92c0f9552e6022015530268dd6314a")
                .build();
        plansSelligent.add(oneMonthPlanSelligent);

        PlanSelligent sixMonthPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 6M")
                .price("c92c0f9552e6022015530268ea6316d")
                .build();
        plansSelligent.add(sixMonthPlanSelligent);

        PlanSelligent oneYearPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 12M")
                .price("c92c0f9552e6022015530268f953190")
                .build();
        plansSelligent.add(oneYearPlanSelligent);

        PlanSelligent twoYearsPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 24M")
                .price("c92c0f9552e60220155302690b631b3")
                .build();
        plansSelligent.add(twoYearsPlanSelligent);

        ListPlan listPlan = ListPlan.Builder.aListPlan()
                .planSelligent(plansSelligent)
                .build();

        return listPlan;
    }

    public static ListPlan discountSelligentAnyMalformedPlan() {

        List<PlanSelligent> plansSelligent = new ArrayList<>();

        PlanSelligent oneMonthPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 1M")
                .price("c92c0f9552e6022015530268dd6314a")
                .build();
        plansSelligent.add(oneMonthPlanSelligent);

        plansSelligent.add(null);

        PlanSelligent oneYearPlanSelligent = PlanSelligent.Builder.aPlanSelligent()
                .name("Default - 12M")
                .price("c92c0f9552e6022015530268f953190")
                .build();
        plansSelligent.add(oneYearPlanSelligent);

        plansSelligent.add(null);

        ListPlan listPlan = ListPlan.Builder.aListPlan()
                .planSelligent(plansSelligent)
                .build();

        return listPlan;
    }

}
