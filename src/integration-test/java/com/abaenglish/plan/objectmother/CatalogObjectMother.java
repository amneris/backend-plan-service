package com.abaenglish.plan.objectmother;

import com.abaenglish.external.zuora.rest.domain.catalog.Catalog;
import com.abaenglish.external.zuora.rest.domain.catalog.Product;
import com.abaenglish.external.zuora.rest.domain.catalog.ProductRatePlan;
import com.abaenglish.external.zuora.rest.domain.catalog.ProductRatePlanCharge;

import java.util.ArrayList;
import java.util.List;

public class CatalogObjectMother {

    public static Catalog defaultCatalog() {

        //ONE MONTH
        ProductRatePlanCharge oneMonthProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("1")
                .name("Reccurring charge")
                .type("Reccurring")
                .model("PerUnit")
                .pricing(PricingObjectMother.getPricings())
                .build();

        List<ProductRatePlanCharge> oneProductsRatePlanChargess = new ArrayList<>();
        oneProductsRatePlanChargess.add(oneMonthProductRatePlanCharge);

        ProductRatePlan oneMonthProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("2c92c0f9552e6022015530268dd6314a")
                .status("Active")
                .name("1 Month")
                .featured(false)
                .period("1")
                .key("1_month")
                .productRatePlanCharges(oneProductsRatePlanChargess)
                .build();

        //SIX MONTH
        ProductRatePlanCharge sixMonthProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("2")
                .name("Reccurring charge")
                .type("Reccurring")
                .model("PerUnit")
                .pricing(PricingObjectMother.getPricings())
                .build();

        List<ProductRatePlanCharge> sixProductsRatePlanCharges = new ArrayList<>();
        sixProductsRatePlanCharges.add(sixMonthProductRatePlanCharge);

        ProductRatePlan sixMonthsProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("2c92c0f9552e6022015530268ea6316d")
                .status("Active")
                .name("6 Months")
                .featured(false)
                .period("6")
                .key("6_months")
                .productRatePlanCharges(sixProductsRatePlanCharges)
                .build();

        //ONE YEAR
        ProductRatePlanCharge oneYearProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("3")
                .name("Reccurring charge")
                .type("Reccurring")
                .model("PerUnit")
                .pricing(PricingObjectMother.getPricings())
                .build();

        List<ProductRatePlanCharge> oneYearProductsRatePlanCharges = new ArrayList<>();
        oneYearProductsRatePlanCharges.add(oneYearProductRatePlanCharge);

        ProductRatePlan oneYearProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("2c92c0f9552e6022015530268f953190")
                .status("Active")
                .name("12 Months")
                .featured(true)
                .period("12")
                .key("12_months")
                .productRatePlanCharges(oneYearProductsRatePlanCharges)
                .build();

        //TWO YEARS
        ProductRatePlanCharge twoYearProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("4")
                .name("Reccurring charge")
                .type("Reccurring")
                .model("PerUnit")
                .pricing(PricingObjectMother.getPricings())
                .build();

        List<ProductRatePlanCharge> twoYearProductsRatePlanCharges = new ArrayList<>();
        twoYearProductsRatePlanCharges.add(twoYearProductRatePlanCharge);

        ProductRatePlan twoYearProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("2c92c0f9552e60220155302690b631b3")
                .status("Active")
                .name("24 Months")
                .featured(false)
                .period("24")
                .key("24_months")
                .productRatePlanCharges(twoYearProductsRatePlanCharges)
                .build();

        List<ProductRatePlan> productsRatePlans = new ArrayList<>();
        productsRatePlans.add(oneMonthProductRatePlan);
        productsRatePlans.add(sixMonthsProductRatePlan);
        productsRatePlans.add(oneYearProductRatePlan);
        productsRatePlans.add(twoYearProductRatePlan);

        Product product = Product.Builder.aProduct()
                .id("1")
                .sku("SKU-0000001")
                .name("ABA English")
                .productRatePlans(productsRatePlans)
                .build();

        List<Product> products = new ArrayList<>();
        products.add(product);

        Catalog catalog = Catalog.Builder.aCatalog()
                .success(true)
                .products(products)
                .build();

        return catalog;
    }

    public static Catalog defaultCatalogABA() {

        //ONE MONTH
        ProductRatePlanCharge oneMonthProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("1")
                .name("Reccurring charge")
                .type("Reccurring")
                .model("PerUnit")
                .pricing(PricingObjectMother.getPricings())
                .build();

        List<ProductRatePlanCharge> oneProductsRatePlanChargess = new ArrayList<>();
        oneProductsRatePlanChargess.add(oneMonthProductRatePlanCharge);

        ProductRatePlan oneMonthProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("301_30|@|301|@|30")
                .status("Active")
                .name("1 Month")
                .featured(false)
                .period("1")
                .key("1_month")
                .productRatePlanCharges(oneProductsRatePlanChargess)
                .build();

        //SIX MONTH
        ProductRatePlanCharge sixMonthProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("2")
                .name("Reccurring charge")
                .type("Reccurring")
                .model("PerUnit")
                .pricing(PricingObjectMother.getPricings())
                .build();

        List<ProductRatePlanCharge> sixProductsRatePlanCharges = new ArrayList<>();
        sixProductsRatePlanCharges.add(sixMonthProductRatePlanCharge);

        ProductRatePlan sixMonthsProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("301_180|@|301|@|180")
                .status("Active")
                .name("6 Months")
                .featured(false)
                .period("6")
                .key("6_months")
                .productRatePlanCharges(sixProductsRatePlanCharges)
                .build();

        //ONE YEAR
        ProductRatePlanCharge oneYearProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("3")
                .name("Reccurring charge")
                .type("Reccurring")
                .model("PerUnit")
                .pricing(PricingObjectMother.getPricings())
                .build();

        List<ProductRatePlanCharge> oneYearProductsRatePlanCharges = new ArrayList<>();
        oneYearProductsRatePlanCharges.add(oneYearProductRatePlanCharge);

        ProductRatePlan oneYearProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("301_360|@|301|@|360")
                .status("Active")
                .name("12 Months")
                .featured(true)
                .period("12")
                .key("12_months")
                .productRatePlanCharges(oneYearProductsRatePlanCharges)
                .build();

        //TWO YEARS
        ProductRatePlanCharge twoYearProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("4")
                .name("Reccurring charge")
                .type("Reccurring")
                .model("PerUnit")
                .pricing(PricingObjectMother.getPricings())
                .build();

        List<ProductRatePlanCharge> twoYearProductsRatePlanCharges = new ArrayList<>();
        twoYearProductsRatePlanCharges.add(twoYearProductRatePlanCharge);

        ProductRatePlan twoYearProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("301_720|@|301|@|720")
                .status("Active")
                .name("24 Months")
                .featured(false)
                .period("24")
                .key("24_months")
                .productRatePlanCharges(twoYearProductsRatePlanCharges)
                .build();

        List<ProductRatePlan> productsRatePlans = new ArrayList<>();
        productsRatePlans.add(oneMonthProductRatePlan);
        productsRatePlans.add(sixMonthsProductRatePlan);
        productsRatePlans.add(oneYearProductRatePlan);
        productsRatePlans.add(twoYearProductRatePlan);

        Product product = Product.Builder.aProduct()
                .id("1")
                .sku("SKU-0000001")
                .name("ABA English")
                .productRatePlans(productsRatePlans)
                .build();

        List<Product> products = new ArrayList<>();
        products.add(product);

        Catalog catalog = Catalog.Builder.aCatalog()
                .success(true)
                .products(products)
                .build();

        return catalog;
    }

    public static Catalog defaultwithDiscountCatalog() {

        Catalog catalog = defaultCatalog();

        //SIX MONTH DISCOUNT
        ProductRatePlanCharge sixMonthDiscountProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("1")
                .name("Discount 20% 6M")
                .type("Reccurring")
                .model("DiscountPercentage")
                .pricing(PricingObjectMother.getPricings20Discount())
                .build();

        List<ProductRatePlanCharge> sixProductsRatePlanCharges = new ArrayList<>();
        sixProductsRatePlanCharges.add(sixMonthDiscountProductRatePlanCharge);

        ProductRatePlan sixMonthsProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("2c92c0f8552e5302015530abaa915b5c")
                .status("Active")
                .name("Discount 20% 6M")
                .key("discount")
                .productRatePlanCharges(sixProductsRatePlanCharges)
                .build();

        //ONE YEAR DISCOUNT
        ProductRatePlanCharge oneYearProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("1")
                .name("Discount 20% 12M")
                .type("Reccurring")
                .model("DiscountPercentage")
                .pricing(PricingObjectMother.getPricings20Discount())
                .build();

        List<ProductRatePlanCharge> oneYearProductsRatePlanCharges = new ArrayList<>();
        oneYearProductsRatePlanCharges.add(oneYearProductRatePlanCharge);

        ProductRatePlan oneYearProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("2c92c0f8552e530a015530aab0645c31")
                .status("Active")
                .name("Discount 20% 12M")
                .key("discount")
                .productRatePlanCharges(oneYearProductsRatePlanCharges)
                .build();

        //TWO YEAR DISCOUNT
        ProductRatePlanCharge twoYearProductRatePlanCharge = ProductRatePlanCharge.Builder.aProductRatePlanCharge()
                .id("1")
                .name("Discount 20% 24M")
                .type("Reccurring")
                .model("DiscountPercentage")
                .pricing(PricingObjectMother.getPricings20Discount())
                .build();

        List<ProductRatePlanCharge> twoYearProductsRatePlanCharges = new ArrayList<>();
        twoYearProductsRatePlanCharges.add(twoYearProductRatePlanCharge);

        ProductRatePlan twoYearProductRatePlan = ProductRatePlan.Builder.aProductRatePlan()
                .id("2c92c0f8552e535a015530a8706400fe")
                .status("Active")
                .name("Discount 20% 24M")
                .key("discount")
                .productRatePlanCharges(twoYearProductsRatePlanCharges)
                .build();

        List<ProductRatePlan> productsRatePlans = new ArrayList<>();
        productsRatePlans.add(sixMonthsProductRatePlan);
        productsRatePlans.add(oneYearProductRatePlan);
        productsRatePlans.add(twoYearProductRatePlan);

        catalog.getProducts().get(0).getProductRatePlans().addAll(productsRatePlans);

        return catalog;
    }

}
