package com.abaenglish.plan.controller.v1.dto.plan;

import java.util.ArrayList;
import java.util.List;

public class PriceList {

    private List<Plan> plans = new ArrayList<>();
    private CurrencyResponse currency;

    public PriceList() {
    }

    public List<Plan> getPlans() {
        return plans;
    }

    public void setPlans(List<Plan> plans) {
        this.plans = plans;
    }

    public CurrencyResponse getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyResponse currencyResponse) {
        this.currency = currencyResponse;
    }
}
