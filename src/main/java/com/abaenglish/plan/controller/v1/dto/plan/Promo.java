package com.abaenglish.plan.controller.v1.dto.plan;

import java.math.BigDecimal;

public class Promo {

    private String discountId;

    private BigDecimal discount;

    private BigDecimal discountMonthly;

    private BigDecimal discountBase;

    public Promo() {
    }

    public String getDiscountId() {
        return discountId;
    }

    public void setDiscountId(String discountId) {
        this.discountId = discountId;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getDiscountMonthly() {
        return discountMonthly;
    }

    public void setDiscountMonthly(BigDecimal discountMonthly) {
        this.discountMonthly = discountMonthly;
    }

    public BigDecimal getDiscountBase() {
        return discountBase;
    }

    public void setDiscountBase(BigDecimal discountBase) {
        this.discountBase = discountBase;
    }
}
