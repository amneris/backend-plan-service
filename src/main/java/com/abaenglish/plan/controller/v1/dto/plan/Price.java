package com.abaenglish.plan.controller.v1.dto.plan;

import java.math.BigDecimal;

public class Price {

    private BigDecimal monthlyBase;
    private BigDecimal base;
    private Promo promo;

    public Price() {
    }

    public BigDecimal getMonthlyBase() {
        return monthlyBase;
    }

    public void setMonthlyBase(BigDecimal monthlyBase) {
        this.monthlyBase = monthlyBase;
    }

    public BigDecimal getBase() {
        return base;
    }

    public void setBase(BigDecimal base) {
        this.base = base;
    }

    public Promo getPromo() {
        return promo;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }
}
