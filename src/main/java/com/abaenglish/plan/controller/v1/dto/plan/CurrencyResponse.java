package com.abaenglish.plan.controller.v1.dto.plan;

public class CurrencyResponse {

    private String symbol;
    private String iso;
    private Boolean symbolShowBeforePrice;
    private String decimalSeparator;

    public CurrencyResponse() {
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getDecimalSeparator() {
        return decimalSeparator;
    }

    public void setDecimalSeparator(String decimalSeparator) {
        this.decimalSeparator = decimalSeparator;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Boolean isSymbolShowBeforePrice() {
        return symbolShowBeforePrice;
    }

    public void setSymbolShowBeforePrice(Boolean symbolShowBeforePrice) {
        this.symbolShowBeforePrice = symbolShowBeforePrice;
    }
}
