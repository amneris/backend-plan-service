package com.abaenglish.plan.controller.v1;

import com.abaenglish.boot.exception.ApiException;
import com.abaenglish.external.zuora.rest.domain.catalog.Catalog;
import com.abaenglish.plan.controller.v1.dto.plan.PriceList;
import com.abaenglish.plan.service.IPlanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "plan", description = "Operations about Catalog of plan")
@RestController
@RequestMapping(value = "api/v1/plans", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PlanController {

    @Autowired
    private IPlanService planService;

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "Get plans filtered by segment", response = Catalog.class)
    public PriceList getCatalogFilteredbySegmentId(
            @RequestParam("user") Long userId,
            @RequestParam(required = false, defaultValue = "0", value = "segment") Integer segmentId) throws ApiException {
        return planService.getPlanList(segmentId, userId);
    }

}
