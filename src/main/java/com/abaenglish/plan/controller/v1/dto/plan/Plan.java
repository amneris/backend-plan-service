package com.abaenglish.plan.controller.v1.dto.plan;

public class Plan {

    private String id;
    private String idProduct;
    private String titleKey;
    private Boolean featured;
    private Integer period;
    private Price prices;

    public Plan() {
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitleKey() {
        return titleKey;
    }

    public void setTitleKey(String titleKey) {
        this.titleKey = titleKey;
    }

    public Boolean isFeatured() {
        return featured;
    }

    public void setFeatured(Boolean featured) {
        this.featured = featured;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Price getPrices() {
        return prices;
    }

    public void setPrices(Price price) {
        this.prices = price;
    }

}
