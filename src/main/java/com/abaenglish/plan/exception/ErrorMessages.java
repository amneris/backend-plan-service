package com.abaenglish.plan.exception;

import com.abaenglish.boot.exception.CodeMessage;

public enum ErrorMessages {

    SELLIGENT_RATE_PLAN_NOT_FOUND_ZUORA(new CodeMessage("SUB0003", "Not found code Selligent in Zuora")),
    CURRENCY_NOT_FOUND(new CodeMessage("SUB0011", "Currency not found"));

    private final CodeMessage error;

    ErrorMessages(CodeMessage errorMessage) {
        error = new CodeMessage(errorMessage.getCode(), errorMessage.getMessage());
    }

    public CodeMessage getError() {
        return error;
    }

}
