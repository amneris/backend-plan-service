package com.abaenglish.plan.service.impl;

import com.abaenglish.boot.exception.NotFoundApiException;
import com.abaenglish.external.selligent.domain.ListPlan;
import com.abaenglish.external.selligent.domain.PlanSelligent;
import com.abaenglish.external.selligent.service.ISelligentSegmentService;
import com.abaenglish.external.selligent.service.properties.SelligentProperties;
import com.abaenglish.external.zuora.rest.domain.catalog.Catalog;
import com.abaenglish.external.zuora.rest.domain.catalog.Pricing;
import com.abaenglish.external.zuora.rest.domain.catalog.Product;
import com.abaenglish.external.zuora.rest.domain.catalog.ProductRatePlan;
import com.abaenglish.external.zuora.rest.service.IZuoraExternalServiceRest;
import com.abaenglish.plan.controller.v1.dto.plan.*;
import com.abaenglish.plan.exception.ErrorMessages;
import com.abaenglish.plan.service.IPlanService;
import com.abaenglish.user.controller.v1.dto.response.CountryIsoApiResponse;
import com.abaenglish.user.controller.v1.dto.response.CurrencyApiResponse;
import com.abaenglish.user.feign.service.IUserServiceFeign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class PlanService implements IPlanService {

    private static final Logger logger = LoggerFactory.getLogger(PlanService.class);

    @Autowired
    private SelligentProperties selligentProperties;

    @Autowired
    private ISelligentSegmentService selligentExternalService;

    @Autowired
    private IZuoraExternalServiceRest zuoraExternalService;

    @Autowired
    private IUserServiceFeign userService;

    @Override
    public PriceList getPlanList(Integer segmentId, Long userId) throws NotFoundApiException {

        ListPlan resultRatePlansSelligent = selligentExternalService.getSelligentSegment(segmentId);

        CurrencyApiResponse currency;

        try {
            currency = userService.getCurrency(userId);
        } catch (Exception e) {
            logger.error("Problem with Feign Client User: " + e);
            throw new NotFoundApiException(ErrorMessages.CURRENCY_NOT_FOUND.getError());
        }

        Catalog catalog = zuoraExternalService.getCatalog();

        CountryIsoApiResponse countryIso = userService.getCountry(userId);

        return filterSelligentwithPlans(resultRatePlansSelligent, catalog, currency, countryIso);
    }

    private PriceList filterSelligentwithPlans(ListPlan listPlan, Catalog catalog, CurrencyApiResponse currency, CountryIsoApiResponse countryIso) throws NotFoundApiException {

        PriceList priceList = new PriceList();

        String moneda = currency.getAbaIdCurrency();
        priceList.setCurrency(getTypeCurrency(moneda));

        List<Plan> planList = new ArrayList<>();

        for (PlanSelligent plansSelligent : listPlan.getPlanSelligent()) {

            Plan planProduct = new Plan();
            Price price = new Price();

            ProductRatePlan searchProductPrice = searchProduct(catalog, plansSelligent.getPrice());

            if (searchProductPrice == null) {
                throw new NotFoundApiException(ErrorMessages.SELLIGENT_RATE_PLAN_NOT_FOUND_ZUORA.getError());
            } else {

                Integer period = Integer.parseInt(searchProductPrice.getPeriod());

                Integer periodDays = period * 30;
                String fieldsSeparator = "|@|"; // 69_180|@|69|@|180

                planProduct.setId(searchProductPrice.getId());
                planProduct.setIdProduct(countryIso.getIdCountry().toString() + "_" + periodDays + fieldsSeparator + countryIso.getIdCountry().toString() + fieldsSeparator + periodDays);

                planProduct.setTitleKey(searchProductPrice.getKey());

                if (searchProductPrice.getFeatured() == null) {
                    planProduct.setFeatured(false);
                } else {
                    planProduct.setFeatured(searchProductPrice.getFeatured());
                }

                planProduct.setPeriod(period);

                BigDecimal base = searchPriceDiscount(
                        searchProductPrice.getRatePlanCharges().get(0).getPricing(),
                        priceList.getCurrency().getIso(),
                        false);

                price.setBase(base.setScale(2));

                price.setMonthlyBase(base.divide(new BigDecimal(searchProductPrice.getPeriod()), 2, BigDecimal.ROUND_DOWN));
                planProduct.setPrices(price);
            }

            ProductRatePlan searchProductDiscount = searchProduct(catalog, plansSelligent.getDiscount());

            if (searchProductDiscount != null) {

                BigDecimal discount = searchPriceDiscount(
                        searchProductDiscount.getRatePlanCharges().get(0).getPricing(),
                        priceList.getCurrency().getIso(),
                        true);

                if (discount != null) {

                    BigDecimal discountBase = planProduct.getPrices().getBase().subtract(planProduct.getPrices().getBase().multiply(discount).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));

                    BigDecimal discountMonthly = planProduct.getPrices().getMonthlyBase().subtract(planProduct.getPrices().getMonthlyBase().multiply(discount).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));

                    Promo promo = new Promo();
                    promo.setDiscountId(searchProductDiscount.getId());
                    promo.setDiscount(discount);
                    promo.setDiscountBase(discountBase.setScale(2, BigDecimal.ROUND_HALF_UP));
                    promo.setDiscountMonthly(discountMonthly.setScale(2, BigDecimal.ROUND_HALF_UP));
                    price.setPromo(promo);
                } else {
                    logger.error("Error search discount ", searchProductDiscount.getRatePlanCharges().get(0).getPricing());
                }

            }

            planList.add(planProduct);

        }
        priceList.setPlans(planList);

        return priceList;
    }

    private ProductRatePlan searchProduct(Catalog catalog, String selligentId) {

        for (Product product : catalog.getProducts()) {

            for (ProductRatePlan productRatePlan : product.getProductRatePlans()) {

                if (selligentId != null && selligentId.equals(productRatePlan.getId())) {
                    return productRatePlan;
                }

            }

        }
        return null;
    }

    private BigDecimal searchPriceDiscount(List<Pricing> listPricing, String currency, boolean discount) {

        BigDecimal base = new BigDecimal(0);
        BigDecimal baseDefault = new BigDecimal(0);

        for (Pricing d : listPricing) {
            if (d.getCurrency() != null && d.getCurrency().contains(selligentProperties.getDefaults().getCurrency())) {
                if (discount) {
                    baseDefault = d.getDiscountPercentage();
                } else {
                    baseDefault = d.getPrice();
                }
            } else if (d.getCurrency() != null && d.getCurrency().contains(currency)) {
                if (discount) {
                    base = d.getDiscountPercentage();
                } else {
                    base = d.getPrice();
                }
            }
        }

        if (base.equals(BigDecimal.ZERO)) {
            base = baseDefault;
        }

        return base;
    }

    private CurrencyResponse getTypeCurrency(String moneda) {

        CurrencyResponse currency = new CurrencyResponse();

        String finalCurrency;

        finalCurrency = (moneda != null) ? moneda : "EUR";

        switch (finalCurrency) {
            case "USD":
                currency.setDecimalSeparator(".");
                currency.setIso("USD");
                currency.setSymbol("US $");
                currency.setSymbolShowBeforePrice(true);
                break;
            case "MXN":
                currency.setDecimalSeparator(".");
                currency.setIso("MXN");
                currency.setSymbol("MXN");
                currency.setSymbolShowBeforePrice(false);
                break;
            case "BRL":
                currency.setDecimalSeparator(".");
                currency.setIso("BRL");
                currency.setSymbol("R$");
                currency.setSymbolShowBeforePrice(true);
                break;
            case "EUR":
            default:
                currency.setDecimalSeparator(",");
                currency.setIso("EUR");
                currency.setSymbol("\u20ac");
                currency.setSymbolShowBeforePrice(false);
                break;
        }

        return currency;
    }
}
