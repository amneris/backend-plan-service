package com.abaenglish.plan.service;

import com.abaenglish.boot.exception.NotFoundApiException;
import com.abaenglish.plan.controller.v1.dto.plan.PriceList;

public interface IPlanService {

    PriceList getPlanList(Integer segmentId, Long userId) throws NotFoundApiException;
}
