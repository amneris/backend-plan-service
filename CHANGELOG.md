# Change Log

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased

## [1.0.0]
### Add
- Created new microservice plan service
- Added new endpoint /plans 
- Added all unit tests
- Separate this microservice from subscription service

